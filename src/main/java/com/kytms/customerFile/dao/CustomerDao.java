package com.kytms.customerFile.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 *
 * 客户档案DAO
 *
 * @author 陈小龙
 * @create 2018-1-5
 */

public interface CustomerDao<Customer> extends BaseDao<Customer> {
}
